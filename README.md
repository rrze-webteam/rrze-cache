# RRZE Cache

Erweitertes Cache WordPress Plugin.


## Beschreibung
RRZE Cache optimiert das Laden von Seiten, indem Beiträge, Seiten und Custom-Post-Types als statischer Inhalt zwischengespeichert werden. Das Plugin verwendet das Drop-In "advanced-cache.php".


## Eigenschaften

- Funktioniert auch mit Custom-Post-Types
- Cache-Dateien werden im Verzeichnis "wp-content/advanced-cache/" gespeichert
- Der geleerte Cache wird vorübergehend im Verzeichnis "wp-content/advanced-cache-old/" gespeichert
- Schaltfläche "Cache leeren" in der WordPress-Adminleiste
- WordPress Multisite kompatibel
- Einzelne Posts können aus dem Cache ausgeschlossen werden
- Manuelles und automatisches Zurücksetzen des Caches.
- Automatische Cache-Verwaltung
- Erweiterbarkeit über Hooks


## Einstellungen

Die Konstante 'WP_CACHE' muss in der Datei 'wp-config.php' definiert und auf 'true' gesetzt werden.
<pre>
define('WP_CACHE', true);
</pre>

Einstellungsmenü: Einstellungen › Cache


## RRZE Cache in einer WP Multi-Site-Installation

Wenn RRZE Cache unter einer WordPress Multisite-Installation installiert wird, wird es für alle Websites aktiviert. Die zentralisierten Einstellungen für das Plugin können nur von einem Superadministrator geändert werden, der auf der Netzwerkverwaltung ausgeführt wird. Die Administratoren einer Website können in diesem Fall nur einige lokale Einstellungen ändern.

Einstellungsmenü: Netzwerkverwaltung › Einstellungen › Cache


## Hooks

Der Filter 'rrzecache_skip_cache' kann den Cache umgehen, wenn er auf true gesetzt ist.

Anwendungsbeispiel:
<pre>
add_filter('rrzecache_skip_cache', '__return_true');
</pre>

Die Aktion 'rrzecache_flush_cache_post_id' leert den Cache einer einzelnen Post-ID.

Anwendungsbeispiel:
<pre>
do_action('rrzecache_flush_cache_post_id', $post->ID);
</pre>

Die Aktion 'rrzecache_flush_cache_url' leert den Cache einer Seiten-URL.

Anwendungsbeispiel:
<pre>
do_action('rrzecache_flush_cache_url', $url);
</pre>

Die Aktion 'rrzecache_flush_cache' leert den Cache der gesamten Website.

Anwendungsbeispiel:
<pre>
do_action('rrzecache_flush_cache');
</pre>
