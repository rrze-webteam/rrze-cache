<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

class XProcess
{
    private int $pid = 0;

    private string $command;

    public function __construct(string $command = '')
    {
        if ($command != '') {
            $this->command = $command;
        }
    }

    private function runCom()
    {
        $command = $this->command . ' > /dev/null 2>&1 & echo $!';
        @exec($command, $op);
        if (isset($op[0])) {
            $this->pid = (int) $op[0];
        }
    }

    public function setPid(int $pid)
    {
        $this->pid = $pid;
    }

    public function getPid()
    {
        return $this->pid;
    }

    public function status(): bool
    {
        if ($this->pid == 0) {
            return false;
        }
        $command = 'ps -p ' . $this->pid;
        @exec($command, $op);
        if (isset($op[1])) {
            return true;
        } else {
            return false;
        }
    }

    public function start()
    {
        if ($this->command != '') {
            $this->runCom();
        }
    }

    public function stop()
    {
        if ($this->pid == 0) {
            return false;
        }
        $command = 'kill ' . $this->pid;
        @exec($command);
        if ($this->status() == false) {
            return true;
        } else {
            return false;
        }
    }
}
