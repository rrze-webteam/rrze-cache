<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

final class File
{
    /**
     * Advanced cache file.
     * @var string
     */
    private static $cacheFile = WP_CONTENT_DIR . '/advanced-cache.php';

    /**
     * Cache directory.
     * @var string
     */
    private static $cacheDir = WP_CONTENT_DIR . '/advanced-cache';

    /**
     * Cache old directory.
     * @var string
     */
    private static $cacheOldDir = WP_CONTENT_DIR . '/advanced-cache-old';

    /**
     * Cache empty directory.
     * @var string
     */
    private static $cacheEmptyDir = WP_CONTENT_DIR . '/advanced-cache-empty';

    /**
     * Timeout file.
     * @var string
     */
    private static $timeoutFile = WP_CONTENT_DIR . '/advanced-cache/.timeout';

    /**
     * Create a symlink.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function createSymlink()
    {
        if (!file_exists(self::$cacheFile)) {
            $rrzeCacheFile = plugin()->getPath() . 'advanced-cache.php';
            return symlink($rrzeCacheFile, self::$cacheFile);
        }
        return true;
    }

    /**
     * Remove a symlink.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function removeSymlink(): bool
    {
        if (@unlink(self::$cacheFile)) {
            return true;
        }
        return false;
    }

    /**
     * Remove HTML file.
     * @param  string $path The file path.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function removeFileHtml(string $path): bool
    {
        $path = self::filePath($path) . 'index.html';
        if (@unlink($path)) {
            return true;
        }
        return false;
    }

    /**
     * Create the cache directory.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function createCacheDir(): bool
    {
        if (file_exists(self::$cacheDir)) {
            return true;
        }
        if (!wp_mkdir_p(self::$cacheDir)) {
            return false;
        }
        return true;
    }

    /**
     * Create the cache old directory.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function createCacheOldDir(): bool
    {
        if (file_exists(self::$cacheOldDir)) {
            return true;
        } elseif (!wp_mkdir_p(self::$cacheOldDir)) {
            return false;
        }
        return true;
    }

    /**
     * Create the cache empty directory.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function createCacheEmptyDir(): bool
    {
        if (file_exists(self::$cacheEmptyDir)) {
            return true;
        } elseif (!wp_mkdir_p(self::$cacheEmptyDir)) {
            return false;
        }
        return true;
    }

    /**
     * Store timeout file.
     * @param  integer $cacheExpires The cache expires in minutes.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function storeTimeout(int $cacheExpires = 10): bool
    {
        $cacheExpires = absint($cacheExpires) ? absint($cacheExpires) : 10;
        return self::createTimeoutFile($cacheExpires);
    }

    /**
     * Create the timeout file.
     * @param  integer $cacheExpires The cache expires in minutes.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    private static function createTimeoutFile(int $cacheExpires): bool
    {
        if (!$handle = @fopen(self::$timeoutFile, 'wb')) {
            do_action(
                'rrze.log.error',
                '{plugin}: The timeout cache file could not be written.',
                [
                    'plugin' => 'rrze-cache',
                    'method' => __METHOD__,
                    '$timeoutFile' => self::$timeoutFile
                ]
            );
            return false;
        }

        fwrite($handle, $cacheExpires);
        fclose($handle);

        self::fileChmod(self::$timeoutFile);

        return true;
    }

    /**
     * Store cache item.
     * @param  string $data The data to store.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function storeItem(string $data = ''): bool
    {
        if (empty($data)) {
            do_action(
                'rrze.log.error',
                '{plugin}: Can not save cache item. The data is empty.',
                [
                    'plugin' => 'rrze-cache',
                    'method' => __METHOD__
                ]
            );
            return false;
        }

        return self::createFiles($data . self::cacheSignatur());
    }

    /**
     * Get cache item.
     * @param  integer $cacheExpires The cache expires in minutes.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function getItem(int $cacheExpires = 0): bool
    {
        $cacheExpires = absint($cacheExpires) ? absint($cacheExpires) : 1;
        $expires_seconds = $cacheExpires * MINUTE_IN_SECONDS;
        if (is_readable(self::fileHtml()) && ((filemtime(self::fileHtml()) + $expires_seconds) > time())) {
            return true;
        }
        return false;
    }

    /**
     * Delete cache item.
     * @param  string $url The URL to delete.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function deleteItem(string $url = ''): bool
    {
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            do_action(
                'rrze.log.error',
                '{pluginn}: The URL is not valid.',
                [
                    'plugin' => 'rrze-cache',
                    'method' => __METHOD__
                ]
            );
            return false;
        }

        $urlPath = untrailingslashit(parse_url(($url ? $url : $_SERVER['REQUEST_URI']), PHP_URL_PATH));
        return self::removeDir(self::filePath($urlPath));
    }

    /**
     * Clear cache data.
     * @param  boolean $all TRUE: All cache data, FALSE: Only current cache data.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    public static function clearData($all = false): bool
    {
        $sourceDir = $all ? self::$cacheDir : self::dirPath();
        $targetDir = self::$cacheOldDir .
            DIRECTORY_SEPARATOR .
            bin2hex(random_bytes(8));

        return self::renameDir($sourceDir, $targetDir);
    }

    /**
     * Cache signature.
     * @return string [description]
     */
    private static function cacheSignatur(): string
    {
        return sprintf(
            "\n\n<!-- %s -->",
            date_i18n(
                __('Y-m-d H:i:s', 'rrze-cache'),
                current_time('timestamp')
            )
        );
    }

    /**
     * Create cache files.
     * @param  string $data The data to store.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    private static function createFiles(string $data): bool
    {
        $filePath = self::filePath();
        if (!$filePath || !wp_mkdir_p($filePath)) {
            return false;
        }

        self::writeFile(self::fileHtml(), $data);
        return true;
    }

    /**
     * Write a cache file.
     * @param  string $file File path.
     * @param  string $data Data to write.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    private static function writeFile(string $file, string $data): bool
    {
        if (!$handle = @fopen($file, 'wb')) {
            do_action(
                'rrze.log.error',
                '{plugin}: The cache file could not be written.',
                [
                    'plugin' => 'rrze-cache',
                    'method' => __METHOD__,
                    '$file' => $file
                ]
            );
            return false;
        }

        @fwrite($handle, $data);
        fclose($handle);

        return self::fileChmod($file);
    }

    /**
     * Cache file chmod.
     * @param  string $file File path.
     * @return boolean TRUE on success, otherwise FALSE.
     */
    private static function fileChmod(string $file): bool
    {
        $stat = @stat(dirname($file));
        $perms = $stat['mode'] & 0007777;
        $perms = $perms & 0000666;
        $return = @chmod($file, $perms);
        return $return;
    }

    /**
     * Directory path.
     * @return mixed The directory path or FALSE.
     */
    private static function dirPath()
    {
        $path = self::$cacheDir .
            DIRECTORY_SEPARATOR .
            parse_url('http://' . strtolower($_SERVER['HTTP_HOST']), PHP_URL_HOST);

        if (validate_file($path) > 0) {
            return false;
        }

        return $path;
    }

    /**
     * File path.
     * @param  string $path The file path.
     * @return mixed The file path or FALSE.
     */
    private static function filePath(string $path = '')
    {
        $path = self::$cacheDir .
            DIRECTORY_SEPARATOR .
            parse_url('http://' . strtolower($_SERVER['HTTP_HOST']), PHP_URL_HOST) .
            parse_url(($path ? $path : $_SERVER['REQUEST_URI']), PHP_URL_PATH);

        if (validate_file($path) > 0) {
            return false;
        }

        return trailingslashit($path);
    }

    /**
     * Get the HTML cached file.
     * @return string The HTML cached file.
     */
    private static function fileHtml(): string
    {
        return self::filePath() . 'index.html';
    }

    /**
     * Rename a directory.
     * @param string $source The source directory path.
     * @param string $target The target directory path.
     * @return bool TRUE on success, otherwise FALSE.
     */
    private static function renameDir(string $source, string $target): bool
    {
        $result = false;
        // Handle arguments.
        if (empty($source) || !file_exists($source)) {
            return true; // No such file/folder exists.
        } elseif (@is_file($source) || @is_link($source)) {
            $result = true; // Rename file/link.
        } elseif (@is_dir($source)) {
            $result = true; // Rename directory.
        }
        return ($result ? @rename($source, $target) : false);
    }

    /**
     * Recursively deletes directories and files contained in a specified directory.
     * @param string $folder The directory path to remove.
     * @return bool TRUE on success, otherwise FALSE.
     */
    public static function removeDir(string $folder): bool
    {
        // Handle arguments.
        if (empty($folder) || !file_exists($folder)) {
            return true; // No such file/folder exists.
        }

        if (!file_exists(self::$cacheEmptyDir)) {
            return true; // No such file/folder exists.
        }

        $command = sprintf(
            'rsync -a --delete %1$s/ %2$s/',
            self::$cacheEmptyDir,
            untrailingslashit($folder)
        );

        $process = new XProcess($command);
        $process->start();
        if ($process->status()) {
            return true;
        }
        return false;
    }

    /**
     * Get the cache file.
     * @return string The cache file.
     */
    public static function getCacheFile(): string
    {
        return self::$cacheFile;
    }

    /**
     * Get the cache directory.
     * @return string The cache directory.
     */
    public static function getCacheDir(): string
    {
        return self::$cacheDir;
    }

    /**
     * Get the cache old directory.
     * @return string The cache old directory.
     */
    public static function getCacheOldDir(): string
    {
        return self::$cacheOldDir;
    }

    /**
     * Get the timeout file.
     * @return string The timeout file.
     */
    public static function getTimeoutFile(): string
    {
        return self::$timeoutFile;
    }
}
