<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

class Schedule
{
    /**
     * Loaded method
     */
    public function loaded()
    {
        if (is_main_site()) {
            add_action('wp', [$this, 'activateScheduledEvents']);
            add_filter('cron_schedules', [$this, 'customCronSchedules']);
            add_action('rrze_cache_every10minutes_event', [$this, 'every10MinutesEvent']);
        } elseif (wp_next_scheduled('rrze_cache_every10minutes_event')) {
            wp_clear_scheduled_hook('rrze_cache_every10minutes_event');
        } else {
            add_filter('cron_schedules', [$this, 'removeCustomCronSchedules']);
        }
    }

    /**
     * Add custom cron schedules.
     * @param array $schedules Available cron schedules
     * @return array New cron schedules
     */
    public function customCronSchedules(array $schedules): array
    {
        $schedules['rrze_cache_every10minutes'] = [
            'interval' => 10 * MINUTE_IN_SECONDS,
            'display' => __('Every 10 minutes', 'rrze-cache')
        ];
        return $schedules;
    }

    /**
     * Remove custom cron schedules.
     * @param array $schedules Available cron schedules
     * @return array New cron schedules
     */
    public function removeCustomCronSchedules(array $schedules): array
    {
        if (isset($schedules['rrze_cache_every10minutes'])) {
            unset($schedules['rrze_cache_every10minutes']);
        }
        return $schedules;
    }

    /**
     * activateScheduledEvents
     * Activate all scheduled events.
     * @return void
     */
    public function activateScheduledEvents()
    {
        if (!wp_next_scheduled('rrze_cache_every10minutes_event')) {
            wp_schedule_event(time(), 'rrze_cache_every10minutes', 'rrze_cache_every10minutes_event');
        }
    }

    /**
     * every10MinutesEvent
     * Run the event every 10 minutes.
     * @return void
     */
    public function every10MinutesEvent()
    {
        File::removeDir(File::getCacheOldDir());
    }
}
