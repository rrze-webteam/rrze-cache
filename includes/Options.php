<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

class Options
{
    /**
     * [protected description]
     * @var string
     */
    protected static $optionName = 'rrze_cache';

    /**
     * [protected description]
     * @var string
     */
    protected static $siteOptionName = 'rrze_cache_site';

    /**
     * [defaultOptions description]
     * @return array [description]
     */
    protected static function defaultOptions()
    {
        $options = [
            'cache_enabled' => 1,
            'cache_expires' => 10, // [minute]
            'cache_skip_on_ip' => ''
        ];

        return $options;
    }

    /**
     * [defaultSiteOptions description]
     * @return array [description]
     */
    protected static function defaultSiteOptions()
    {
        $options = [
            'cache_enabled' => 0,
            'cache_expires' => 10, // [minute]
            'cache_skip_on_ip' => ''
        ];

        return $options;
    }

    /**
     * [getOptions description]
     * @return object [description]
     */
    public static function getOptions()
    {
        $defaults = self::defaultOptions();

        $options = (array) get_option(self::$optionName);
        $options = wp_parse_args($options, $defaults);
        $options = array_intersect_key($options, $defaults);

        return (object) $options;
    }

    /**
     * [getSiteOptions description]
     * @return object [description]
     */
    public static function getSiteOptions()
    {
        $defaults = self::defaultSiteOptions();

        $options = (array) get_site_option(self::$siteOptionName);
        $options = wp_parse_args($options, $defaults);
        $options = array_intersect_key($options, $defaults);

        return (object) $options;
    }

    /**
     * [getOptionName description]
     * @return string [description]
     */
    public static function getOptionName()
    {
        return self::$optionName;
    }

    /**
     * [getSiteOptionName description]
     * @return string [description]
     */
    public static function getSiteOptionName()
    {
        return self::$siteOptionName;
    }
}
