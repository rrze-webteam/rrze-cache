<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

class Main
{
    /**
     * @var object
     */
    protected $siteOptions;

    /**
     * @var boolean
     */
    protected $isPluginActiveForNetwork;

    public function __construct()
    {
        $this->siteOptions = Options::getSiteOptions();

        $this->isPluginActiveForNetwork = $this->isPluginActiveForNetwork(plugin()->getBaseName());
    }

    public function loaded()
    {
        $settings = new Settings;
        $settings->loaded();

        $schedule = new Schedule;
        $schedule->loaded();

        if ($this->isPluginActiveForNetwork && !$this->siteOptions->cache_enabled) {
            return;
        }

        if (is_main_site(get_current_blog_id())) {
            File::createSymlink();
        }

        $cache = new Cache;
        $cache->loaded();

        // add_filter('robots_txt', [$this, 'robotsTxt'], 10, 2);
        add_action('admin_enqueue_scripts', [$this, 'adminEnqueueScripts']);
        // Enqueue Block Editor Assets
        add_action('enqueue_block_editor_assets', [$this, 'enqueueBlockEditorAssets']);
    }

    public function robotsTxt($output, $public)
    {
        if ($public) {
            $path = parse_url(site_url(), PHP_URL_PATH);
            $output .= sprintf('%2$sDisallow: %1$s/wp-content/advanced-cache/%2$s', (empty($path) ? '' : $path), PHP_EOL);
        }
        return $output;
    }

    public function adminEnqueueScripts($hook)
    {
        if ($hook == 'settings_page_rrze-cache') {
            wp_enqueue_style(
                'rrze-cache-settings',
                plugins_url('build/settings.css', plugin()->getBasename()),
                [],
                plugin()->getVersion()
            );

            $assetFile = include(plugin()->getPath('build') . 'settings.asset.php');
            wp_enqueue_script(
                'rrze-cache-settings',
                plugins_url('build/settings.js', plugin()->getBasename()),
                $assetFile['dependencies'] ?? [],
                $assetFile['version'] ?? plugin()->getVersion(),
                true
            );

            wp_localize_script(
                'rrze-cache-settings',
                'rrze_cache_vars',
                [
                    'filters_label_1' => __('All', 'rrze-cache'),
                    'filters_label_2' => __('Selected', 'rrze-cache'),
                    'placeholder' => __('Search...', 'rrze-cache')
                ]
            );
        }

        wp_register_style(
            'rrze-cache-post-edit',
            plugins_url('build/post-edit.css', plugin()->getBasename()),
            [],
            plugin()->getVersion()
        );

        $assetFile = include(plugin()->getPath('build') . 'post-edit.asset.php');
        wp_register_script(
            'rrze-cache-post-edit',
            plugins_url('build/post-edit.js', plugin()->getBasename()),
            $assetFile['dependencies'] ?? [],
            $assetFile['version'] ?? plugin()->getVersion(),
            true
        );
    }

    public function enqueueBlockEditorAssets()
    {
        $assetFile = include plugin()->getPath('build') . 'block-editor.asset.php';
        wp_enqueue_script(
            'rrze-cache-block-editor',
            plugins_url('build/block-editor.js', plugin()->getBasename()),
            $assetFile['dependencies'] ?? [],
            $assetFile['version'] ?? plugin()->getVersion(),
            true
        );

        wp_set_script_translations(
            'rrze-cache-block-editor',
            'rrze-cache',
            plugin()->getPath('languages')
        );
    }

    /**
     * Check if the plugin is active for network.
     * @return boolean
     */
    public function isPluginActiveForNetwork(string $plugin): bool
    {
        include_once ABSPATH . 'wp-admin/includes/plugin.php';
        return is_plugin_active_for_network($plugin);
    }
}
