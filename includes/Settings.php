<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

use RRZE\Cache\Utils\File as UFile;
use RRZE\Cache\Utils\IPUtils;

class Settings
{
    /**
     * Options
     * @var object
     */
    protected $options;

    /**
     * Site Option Name
     * @var string
     */
    protected $siteOptionName;

    /**
     * Site Options
     * @var object
     */
    protected $siteOptions;

    /**
     * Is Plugin Active For Network
     * @var boolean
     */
    protected $isPluginActiveForNetwork;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = Options::getOptions();
        $this->siteOptionName = Options::getSiteOptionName();
        $this->siteOptions = Options::getSiteOptions();

        $this->isPluginActiveForNetwork = $this->isPluginActiveForNetwork(plugin()->getBaseName());
    }

    /**
     * Loaded method
     */
    public function loaded()
    {
        if (!$this->isPluginActiveForNetwork || ($this->isPluginActiveForNetwork && $this->siteOptions->cache_enabled)) {
            add_action('admin_init', [$this, 'registerSettings']);
            add_action('admin_menu', [$this, 'adminMenu']);
        }

        if ($this->isPluginActiveForNetwork) {
            add_action('network_admin_menu', [$this, 'networkAdminMenu']);
            add_action('admin_init', [$this, 'networkAdminSettings']);
            add_filter('network_admin_plugin_action_links_' . plugin()->getBasename(), [$this, 'networkAdminPluginActionLinks']);
        } else {
            add_filter('plugin_action_links_' . plugin()->getBasename(), [$this, 'pluginActionLinks']);
        }
    }

    /**
     * Netwok Admin Plugin Action Links
     * @param  array $links Action links
     * @return array Action links
     */
    public function networkAdminPluginActionLinks($links)
    {
        if (!current_user_can('manage_network_options')) {
            return $links;
        }
        return array_merge($links, array(sprintf('<a href="%s">%s</a>', add_query_arg(['page' => 'rrzecache-network'], network_admin_url('settings.php')), __('Settings', 'rrze-cache'))));
    }

    /**
     * Plugin Action Links
     * @param  array $links Action links
     * @return array Action links
     */
    public function pluginActionLinks($links)
    {
        if (!current_user_can('manage_options')) {
            return $links;
        }
        return array_merge($links, array(sprintf('<a href="%s">%s</a>', add_query_arg(['page' => 'rrze-cache'], admin_url('options-general.php')), __('Settings', 'rrze-cache'))));
    }

    /**
     * Admin Menu
     */
    public function adminMenu()
    {
        if (!isset($_GET['settings-updated']) && isset($_GET['update']) && $_GET['update'] == 'flushed') {
            $this->adminNoticeFlushCache();
        }

        add_options_page(__('Cache', 'rrze-cache'), __('Cache', 'rrze-cache'), 'manage_options', 'rrze-cache', [$this, 'optionsPage']);
    }

    /**
     * Register Settings
     */
    public function registerSettings()
    {
        register_setting('rrze-cache', 'rrze_cache', [$this, 'validateOptions']);
    }

    /**
     * Validate Options
     * @param  array $input Input
     * @return array Validated Input
     */
    public function validateOptions($input)
    {
        if (isset($_POST['rrze-cache-flush'])) {
            Flush::flushCache();
            wp_redirect(
                add_query_arg(
                    [
                        'page' => 'rrze-cache',
                        'update' => 'flushed'
                    ],
                    admin_url('options-general.php')
                )
            );
            exit;
        }

        $cacheExceptions = !empty($_POST['rrze_cache_select']) ? $_POST['rrze_cache_select'] : '';
        if (is_array($cacheExceptions)) {
            foreach ($cacheExceptions as $postId) {
                delete_post_meta($postId, RRZECACHE_META_KEY);
            }
        }

        $cacheExpires = $input['cache_expires'] ?? 0;
        $input['cache_expires'] = absint($cacheExpires) >= 1 ? absint($cacheExpires) : $this->options->cache_expires;
        File::storeTimeout($input['cache_expires']);

        $cacheSkipOnIp = $input['cache_skip_on_ip'] ?? '';
        $input['cache_skip_on_ip'] = $this->sanitizeTextareaIpList((string) $cacheSkipOnIp);

        return $input;
    }

    /**
     * Cache Disabled List
     */
    protected function cacheDisabledList()
    {
        global $wpdb;

        $posts = $wpdb->get_results(
            "
            SELECT ID, post_title, post_type FROM $wpdb->posts
            INNER JOIN $wpdb->postmeta ON ($wpdb->posts.ID = $wpdb->postmeta.post_id)
            WHERE $wpdb->posts.post_status = 'publish'
            AND $wpdb->postmeta.meta_key = '" . RRZECACHE_META_KEY . "'
            AND $wpdb->postmeta.meta_value = 'disabled'"
        );

        $postTypes = get_post_types(['public' => true], 'objects');

        // rrze-ac plugin
        $rrzeAcPlugin = $this->isPluginActive('rrze-ac/rrze-ac.php');

        if ($posts) {
            echo '<ul class="rrze-cache-select-list">';
            foreach ($posts as $post) {
                if (!isset($postTypes[$post->post_type])) {
                    continue;
                }
                $postTypeLabel = !empty($postTypes[$post->post_type]) ? $postTypes[$post->post_type]->labels->singular_name : '';
                $rrzeAcPermission = get_post_meta($post->ID, ACCESS_PERMISSION_META_KEY, true);
                $disabled = $rrzeAcPlugin && $rrzeAcPermission && $rrzeAcPermission !== 'public' ? ' disabled' : '';
                $subtitle = $disabled ? sprintf(
                    /* translators: %s: Access protection status. */
                    __('%s (Access protection activated)', 'rrze-cache'),
                    $postTypeLabel
                ) : $postTypeLabel;
                echo '<li>';
                echo '<label for="rrze-cache-selected">';
                echo '<input type="checkbox" id="', esc_attr('rrze-cache-selected-' . $post->ID), '" name="rrze_cache_select[]" value="', esc_attr($post->ID), '"', $disabled, '>';
                echo '<span class="rrze-cache-selected-title"><a href="', get_edit_post_link($post->ID), '">', $post->post_title, '</a></span>';
                echo '<span class="rrze-cache-selected-subtitle">', esc_html($subtitle), '</span>';
                echo '</label>';
                echo '</li>';
            }
            echo '</ul>';
            echo '<p class="description">';
            _e('Select the checkbox to remove an item from the list.', 'rrze-cache');
            echo '</p>';
        } else {
            echo '<p>';
            _e('No items found.', 'rrze-cache');
            echo '</p>';
        }
    }

    /**
     * Options Page
     */
    public function optionsPage()
    {
?>
        <style>
            #rrzecache_settings input[type="text"],
            #rrzecache_settings input[type="number"] {
                height: 30px;
            }
        </style>

        <div class="wrap" id="rrze-cache-settings">
            <h2><?php _e('Cache Settings', 'rrze-cache') ?></h2>

            <form method="post" action="options.php">
                <?php settings_fields('rrze-cache') ?>
                <?php if ($this->isPluginActiveForNetwork) : ?>
                    <input type="hidden" name="rrze_cache[cache_expires]" value="<?php esc_attr_e($this->options->cache_expires) ?>">
                    <input type="hidden" name="rrze_cache[cache_skip_on_ip]" value="<?php esc_attr_e($this->options->cache_skip_on_ip) ?>">
                <?php endif ?>
                <table class="form-table">
                    <?php if (!$this->isPluginActiveForNetwork) : ?>
                        <tr valign="top">
                            <th scope="row">
                                <?php esc_html_e('Lifetime', 'rrze-cache') ?>
                            </th>
                            <td>
                                <label for="rrze-cache-expires">
                                    <input type="number" min="1" step="1" name="rrze_cache[cache_expires]" id="rrze-cache-expires" value="<?php esc_attr_e($this->options->cache_expires) ?>" class="small-text">
                                    <?php echo esc_html(_nx('Minute', 'Minutes', $this->options->cache_expires, 'rrze-cache-expires', 'rrze-cache')) ?>
                                </label>
                            </td>
                        </tr>
                    <?php endif ?>
                    <?php if (!$this->isPluginActiveForNetwork) : ?>
                        <tr valign="top">
                            <th scope="row">
                                <?php esc_html_e('Skip On IP', 'rrze-cache') ?>
                            </th>
                            <td>
                                <textarea id="rrze-cache-skip-on-ip" cols="50" rows="5" name="rrze_cache_site[cache_skip_on_ip]"><?php esc_attr_e($this->options->cache_skip_on_ip); ?></textarea>
                                <p class="description"><?php _e('Add one IP Address per line. The Cache will be skipped if the client has one of these IP Addresses.', 'rrze-cache'); ?></p>
                            </td>
                        </tr>
                    <?php endif ?>
                    <tr valign="top">
                        <th scope="row">
                            <?php esc_html_e('Exemptions', 'rrze-cache') ?>
                        </th>
                        <td>
                            <?php $this->cacheDisabledList() ?>
                        </td>
                    </tr>
                </table>

                <?php submit_button(
                    esc_html__('Save Changes', 'rrze-cache'),
                    'primary',
                    'rrze-cache-submit',
                    false
                ) ?>
                <?php submit_button(
                    esc_html__('Clear cache', 'rrze-cache'),
                    'secondary',
                    'rrze-cache-flush',
                    false
                ) ?>
            </form>
        </div>
    <?php
    }

    /**
     * Enable Cache
     * @return boolean True if the cache is enabled, false otherwise
     */
    protected function enableCache(): bool
    {
        return File::createSymlink();
    }

    /**
     * Disable Cache
     * @return boolean True if the cache is disabled, false otherwise
     */
    protected function disableCache(): bool
    {
        return File::removeSymlink();
    }

    /**
     * Network Admin Menu
     */
    public function networkAdminMenu()
    {
        if (isset($_POST['_wpnonce']) &&  wp_verify_nonce($_POST['_wpnonce'], 'rrze_cache_network-options') && current_user_can('manage_network_options')) {
            if (isset($_POST['rrze-cache-site-submit'])) {
                $cacheError = false;

                $siteCache = isset($_POST['rrze_cache_site']) ? $_POST['rrze_cache_site'] : [];

                $cacheEnabled = !empty($siteCache['cache_enabled']) ? 1 : 0;
                $cacheExpires = !empty($siteCache['cache_expires']) && absint($siteCache['cache_expires']) >= 1 ? absint($siteCache['cache_expires']) : $this->siteOptions->cache_expires;

                $ipAddresses = !empty($siteCache['cache_skip_on_ip']) ? $this->sanitizeTextareaIpList($siteCache['cache_skip_on_ip']) : '';
                $this->siteOptions->cache_skip_on_ip = $ipAddresses;
                $filename = WP_PLUGIN_DIR .
                    DIRECTORY_SEPARATOR .
                    'rrze-cache' .
                    DIRECTORY_SEPARATOR .
                    'skip-on-ip.network';

                $ipAddresses = !empty($ipAddresses) ? explode(PHP_EOL, $ipAddresses) : [];
                if (!empty($ipAddresses) && $skipOnIp = json_encode($ipAddresses)) {
                    UFile::write($filename, $skipOnIp);
                } else {
                    @unlink($filename);
                }

                $enableCache = $this->enableCache();
                if ($cacheEnabled && (!defined('WP_CACHE') || !WP_CACHE)) {
                    if (!$enableCache) {
                        $cacheEnabled = 0;
                        $cacheError = true;
                    }
                } elseif (!$cacheEnabled) {
                    $this->disableCache();
                } else {
                    $cacheEnabled = $this->siteOptions->cache_enabled = $cacheEnabled;
                }

                if (!$cacheError) {
                    $this->siteOptions->cache_enabled = $cacheEnabled;
                    $this->siteOptions->cache_expires = $cacheExpires;
                    File::storeTimeout($cacheExpires);
                    update_site_option($this->siteOptionName, $this->siteOptions);
                }

                $queryVal = 'updated';
            } elseif (isset($_POST['rrze-cache-site-flush'])) {
                Flush::flushAllCache();
                $queryVal = 'flushed';
            }

            wp_redirect(
                add_query_arg(
                    [
                        'page' => 'rrzecache-network',
                        'update' => $queryVal
                    ],
                    network_admin_url('settings.php')
                )
            );
            exit;
        }

        if (isset($_GET['update']) && ($_GET['update'] == 'updated' || $_GET['update'] == 'flushed')) {
            add_action('network_admin_notices', function () {
                $message = $_GET['update'] == 'updated' ? __('Settings saved.', 'rrze-cache') : __('Cache is empty.', 'rrze-cache');
                echo sprintf('<div class="notice notice-success is-dismissible"><p><strong>%s</strong></p></div>', esc_html__($message));
            });
        }

        add_submenu_page('settings.php', __('Cache', 'rrze-cache'), __('Cache', 'rrze-cache'), 'manage_network_options', 'rrzecache-network', [$this, 'networkPageSettings']);
    }

    /**
     * Network Admin Settings
     */
    public function networkAdminSettings()
    {
        add_settings_section('rrze_advanced_cache_section', __('Page Cache (Advanced Cache)', 'rrze-cache'), '__return_false', 'rrze_cache_network');

        add_settings_field('rrze-cache-enable', __('Enable Cache', 'rrze-cache'), [$this, 'cacheEnabledField'], 'rrze_cache_network', 'rrze_advanced_cache_section');
        add_settings_field('rrze-cache-expires', __('Lifetime', 'rrze-cache'), [$this, 'cacheExpiresField'], 'rrze_cache_network', 'rrze_advanced_cache_section');
        add_settings_field('rrze-cache-skip-on-ip', __('Skip On IP', 'rrze-cache'), [$this, 'cacheSkipOnIpField'], 'rrze_cache_network', 'rrze_advanced_cache_section');
    }

    /**
     * Cache Enabled field
     */
    public function cacheEnabledField()
    {
    ?>
        <label>
            <input type="radio" name="rrze_cache_site[cache_enabled]" value="1" <?php checked('1', $this->siteOptions->cache_enabled); ?>><?php esc_html_e('Cache is enabled', 'rrze-cache') ?>
        </label><br>
        <label>
            <input type="radio" name="rrze_cache_site[cache_enabled]" value="0" <?php checked('0', $this->siteOptions->cache_enabled); ?>><?php esc_html_e('Cache is disabled', 'rrze-cache') ?>
        </label>
    <?php
    }

    /**
     * Cache Expires field
     */
    public function cacheExpiresField()
    {
    ?>
        <label for="rrze-cache-expires">
            <input type="number" min="1" step="1" name="rrze_cache_site[cache_expires]" value="<?php echo esc_attr($this->siteOptions->cache_expires) ?>" class="small-text">
            <?php echo esc_html(_nx('Minute', 'Minutes', $this->siteOptions->cache_expires, 'rrze-cache-expires', 'rrze-cache')) ?>
        </label>
    <?php
    }

    /**
     * Cache Skip On IP field
     */
    public function cacheSkipOnIpField()
    {
        echo '<textarea id="rrze-cache-skip-on-ip" cols="50" rows="5" name="rrze_cache_site[cache_skip_on_ip]">', esc_attr($this->siteOptions->cache_skip_on_ip), '</textarea>';
        echo '<p class="description">', __('Add one IP Address per line. The Cache will be skipped if the client has one of these IP Addresses.', 'rrze-cache'), '</p>';
    }

    /**
     * Network page settings
     */
    public function networkPageSettings()
    {
    ?>
        <div class="wrap">
            <h2><?php echo esc_html(__('Cache', 'rrze-cache')); ?></h2>

            <form method="post">
                <?php
                settings_fields('rrze_cache_network');
                do_settings_sections('rrze_cache_network'); ?>
                <?php if ($this->siteOptions->cache_enabled) : ?>
                    <?php submit_button(
                        esc_html__('Save Changes', 'rrze-cache'),
                        'primary',
                        'rrze-cache-site-submit'
                    ) ?>
                <?php else : ?>
                    <?php submit_button(
                        esc_html__('Save Changes', 'rrze-cache'),
                        'primary',
                        'rrze-cache-site-submit',
                        false
                    ) ?>
                    <?php submit_button(
                        esc_html__('Delete Page Cache', 'rrze-cache'),
                        'secondary',
                        'rrze-cache-site-flush',
                        false
                    ) ?>
                <?php endif ?>
            </form>

        </div>
<?php
    }

    /**
     * Admin notice flush cache
     */
    protected function adminNoticeFlushCache()
    {
        add_action('admin_notices', function () {
            echo sprintf('<div class="updated settings-error notice is-dismissible"><p><strong>%s</strong></p></div>', esc_html__(__('Cache is empty.', 'rrze-cache')));
        });
    }

    /**
     * Sanitize textarea IP list
     * @param string $input
     * @return string
     */
    public function sanitizeTextareaIpList(string $input = '')
    {
        if (!empty($input)) {
            $inputAry = explode(PHP_EOL, sanitize_textarea_field($input));
            $inputAry = array_filter(array_map('trim', $inputAry));
            $inputAry = array_unique(array_values($inputAry));
        }
        $inputAry = !empty($inputAry) ? $this->sanitizeIpRange($inputAry) : '';
        return !empty($inputAry) ? implode(PHP_EOL, $inputAry) : '';
    }

    /**
     * Sanitize IP range
     * @param array $ipAddress
     * @return array
     */
    public function sanitizeIpRange(array $ipAddress)
    {
        $ipRange = [];
        if (!empty($ipAddress)) {
            foreach ($ipAddress as $value) {
                $value = trim($value);
                if (empty($value)) {
                    continue;
                }
                $sanitizedValue = IPUtils::sanitizeIpRange($value);
                if (!is_null($sanitizedValue)) {
                    $ipRange[] = $sanitizedValue;
                }
            }
        }
        return $ipRange;
    }

    /**
     * Check if the plugin is active.
     * @return boolean
     */
    public function isPluginActive(string $plugin): bool
    {
        include_once ABSPATH . 'wp-admin/includes/plugin.php';
        return is_plugin_active($plugin);
    }

    /**
     * Check if the plugin is active for network.
     * @return boolean
     */
    public function isPluginActiveForNetwork(string $plugin): bool
    {
        include_once ABSPATH . 'wp-admin/includes/plugin.php';
        return is_plugin_active_for_network($plugin);
    }
}
