<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

use RRZE\Cache\Utils\{IP, RemoteAddress};

class Cache
{
    /**
     * Options
     * @var object
     */
    protected $options;

    /**
     * Site Options
     * @var object
     */
    protected $siteOptions;

    /**
     * Is Plugin Active For Network
     * @var boolean
     */
    protected $isPluginActiveForNetwork;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = Options::getOptions();
        $this->siteOptions = Options::getSiteOptions();

        $this->isPluginActiveForNetwork = $this->isPluginActiveForNetwork(plugin()->getBaseName());
    }

    /**
     * Loaded method
     */
    public function loaded()
    {
        $this->setCacheFiles();

        add_action('init', [__CLASS__, 'registerMetas']);
        add_action('save_post', [$this, 'savePost'], 99, 3);
        add_action('pre_comment_approved', [$this, 'preCommentApproved'], 99, 2);
        add_action('transition_comment_status', [$this, 'transitionCommentStatus'], 10, 3);
        add_action('edit_comment', [$this, 'editComment']);
        add_action('template_redirect', [$this, 'templateRedirect'], 1);

        add_action('rrzecache_flush_cache_post_id', [__NAMESPACE__ . '\Flush', 'flushCacheByPostId']);
        add_action('rrzecache_flush_cache_url', [__NAMESPACE__ . '\Flush', 'flushCacheByUrl']);
        add_action('rrzecache_flush_cache', [__NAMESPACE__ . '\Flush', 'flushCache']);
        add_action('_core_updated_successfully', [__NAMESPACE__ . '\Flush', 'flushAllCache']);
        add_action('switch_theme', [__NAMESPACE__ . '\Flush', 'flushCache']);
        add_action('customize_save_after', [__NAMESPACE__ . '\Flush', 'flushCache']);
        add_action('wp_update_nav_menu', [__NAMESPACE__ . '\Flush', 'flushCache']);
        add_action('wp_trash_post', [__NAMESPACE__ . '\Flush', 'flushCache']);

        add_action('admin_init', function () {
            $screen = get_current_screen();
            if (!$screen || !method_exists($screen, 'is_block_editor')) {
                add_action('post_submitbox_misc_actions', [$this, 'postCacheSubmitbox'], 99);
            }
        });
    }

    /**
     * Set cache files
     */
    protected function setCacheFiles()
    {
        File::createCacheDir();
        File::createCacheOldDir();
        File::createCacheEmptyDir();
        if (!file_exists(File::getTimeoutFile())) {
            File::storeTimeout($this->siteOptions->cache_expires);
        }
    }

    public static function registerMetas()
    {
        $postTypes = get_post_types(['public' => true]);
        foreach ($postTypes as $postType) {
            register_meta(
                $postType,
                RRZECACHE_META_KEY,
                [
                    'show_in_rest' => true,
                    'type' => 'string',
                    'single' => true,
                    'auth_callback' => function () {
                        return current_user_can('edit_posts');
                    },
                    'default' => 'enabled',
                ]
            );
        }
    }

    /**
     * Update/Publish a post
     * @param int $postId
     * @param object $post
     * @param boolean $update
     */
    public function savePost($postId, $post, $update)
    {
        if (
            !wp_is_post_autosave($postId) &&
            $post->post_status == 'publish' &&
            $update
        ) {
            File::removeFileHtml('/');
            Flush::flushCacheByPostId($postId);
        }

        if (
            isset($_POST['post_cache_submitbox_nonce']) &&
            wp_verify_nonce($_POST['post_cache_submitbox_nonce'], 'post_cache_submitbox')
        ) {
            if (
                isset($_POST['rrze_cache_select']) &&
                $_POST['rrze_cache_select'] == 'disabled'
            ) {
                update_post_meta($postId, RRZECACHE_META_KEY, 'disabled');
            } else {
            }
        }

        // rrze-ac plugin
        if (
            is_plugin_active('rrze-ac/rrze-ac.php') &&
            isset($_POST['access_permission_select']) &&
            !in_array($_POST['access_permission_select'], ['_none_', 'public'])
        ) {
            update_post_meta($postId, RRZECACHE_META_KEY, 'disabled');
        }
    }

    /**
     * Pre comment approved
     * @param int $approved
     * @param array $comment
     * @return int
     */
    public function preCommentApproved($approved, $comment)
    {
        if ($approved === 1) {
            Flush::flushCacheByPostId($comment['comment_post_ID']);
        }
        return $approved;
    }

    /**
     * Transition comment status
     * @param string $newStatus
     * @param string $oldStatus
     * @param object $comment
     */
    public function transitionCommentStatus($newStatus, $oldStatus, $comment)
    {
        if ($newStatus != $oldStatus) {
            Flush::flushCacheByPostId($comment->comment_post_ID);
        }
    }

    /**
     * Edit comment
     * @param int $id
     */
    public function editComment($id)
    {
        Flush::flushCacheByPostId(get_comment($id)->comment_post_ID);
    }

    /**
     * Post cache submitbox
     */
    public function postCacheSubmitbox()
    {
        global $post, $post_type, $post_type_object;

        $postTypes = get_post_types(['public' => true]);
        if (!in_array($post_type, $postTypes)) {
            return;
        }

        if (!current_user_can($post_type_object->cap->publish_posts)) {
            return;
        }

        wp_enqueue_style('rrze-cache-post-edit');
        wp_enqueue_script('rrze-cache-post-edit');

        wp_nonce_field('post_cache_submitbox', 'post_cache_submitbox_nonce');

        $cache = get_post_meta($post->ID, RRZECACHE_META_KEY, true);
        $cache = $cache == 'disabled' ?: 'enabled';

        // rrze-ac plugin
        if (is_plugin_active('rrze-ac/rrze-ac.php')) {
            $permission = get_post_meta($post->ID, ACCESS_PERMISSION_META_KEY, true);
            if ($permission && $permission !== 'public') {
                $cache = 'disabled';
            }
        }

        $label = $cache == 'disabled' ? __('Disabled', 'rrze-cache') : __('Enabled', 'rrze-cache');
        $class = $cache == 'disabled' ? 'cache-disabled-icon' : 'cache-icon'; ?>
        <div id="post-cache-wrap" class="misc-pub-section">
            <span>
                <span id="cache-icon" class="<?php echo $class; ?> dashicons dashicons-cloud"></span>
                <?php _e('Cache:', 'rrze-cache'); ?>
                <b id="post-cache-label"><?php echo $label; ?></b>
            </span>
            <?php if (empty($permission) || in_array($permission, ['_none_', 'public'])) : ?>
                <a href="#" id="edit-post-cache" class="edit-post-cache hide-if-no-js">
                    <span aria-hidden="true"><?php _e('Edit', 'rrze-cache'); ?></span>
                    <span class="screen-reader-text"><?php _e('Edit Cache', 'rrze-cache'); ?></span>
                </a>
                <div id="post-cache-field" class="hide-if-js">
                    <select id="rrze-cache-select" name="rrze_cache_select">
                        <option value="enabled" <?php selected($cache, 'enabled'); ?>>
                            <?php echo sanitize_text_field(__('Enable', 'rrze-cache')); ?>
                        </option>
                        <option value="disabled" <?php selected($cache, 'disabled'); ?>>
                            <?php echo sanitize_text_field(__('Disable', 'rrze-cache')); ?>
                        </option>
                    </select>
                    <a href="#" class="save-post-cache hide-if-no-js button"><?php _e('OK', 'rrze-cache'); ?></a>
                    <a href="#" class="cancel-post-cache hide-if-no-js button-cancel"><?php _e('Cancel', 'rrze-cache'); ?></a>
                </div>
            <?php endif ?>
        </div>
<?php
    }

    /**
     * Template redirect
     */
    public function templateRedirect()
    {
        if ($this->skipCache()) {
            return;
        }

        $cached = File::getItem($this->getCacheExpires());

        if (!$cached) {
            ob_start([$this, 'storeCache']);
        }
    }

    /**
     * Get cache expires
     * @return int
     */
    public function getCacheExpires()
    {
        if ($this->isPluginActiveForNetwork) {
            return $this->siteOptions->cache_expires;
        }
        return $this->options->cache_expires;
    }

    /**
     * Store cache
     * @param string $data
     * @return string
     */
    public function storeCache($data)
    {
        if (empty($data)) {
            return '';
        }

        File::storeItem($data);

        return $data;
    }

    /**
     * Skip cache
     * @return boolean
     */
    protected function skipCache()
    {
        if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] != 'GET') {
            return true;
        }

        if (!empty($_SERVER['QUERY_STRING'])) {
            return true;
        }

        if (basename($_SERVER['SCRIPT_NAME']) != 'index.php') {
            return true;
        }

        if ($this->skipOnIp()) {
            return true;
        }

        if ($this->isLoggedIn()) {
            return true;
        }

        if (apply_filters('rrzecache_skip_cache', false)) {
            return true;
        }

        if (is_search() || is_404() || is_feed() || is_trackback() || is_robots() || is_preview() || post_password_required()) {
            return true;
        }

        if (defined('DONOTCACHEPAGE') && DONOTCACHEPAGE) {
            return true;
        }

        if ($this->isMobile()) {
            return true;
        }

        if ($this->isWpSitemap()) {
            return true;
        }

        if (is_singular()) {
            $postId = $GLOBALS['wp_query']->get_queried_object_id();

            $cache = get_post_meta($postId, RRZECACHE_META_KEY, true);
            if ($cache == 'disabled') {
                return true;
            }
        }

        return false;
    }

    /**
     * Is logged in
     * @return boolean
     */
    protected function isLoggedIn()
    {
        if (is_user_logged_in()) {
            return true;
        }

        if (empty($_COOKIE)) {
            return false;
        }

        foreach ($_COOKIE as $k => $v) {
            if (preg_match('/^(wp-postpass|wordpress_logged_in|comment_author)_/', $k)) {
                return true;
            }
        }
    }

    /**
     * Is WP sitemap
     * @return boolean
     */
    protected function isWpSitemap()
    {
        global $wp;
        if (preg_match('/^wp-sitemap/', $wp->request)) {
            return true;
        }
        return false;
    }

    /**
     * Is mobile
     * @return boolean
     */
    protected function isMobile()
    {
        $templatedir = get_template_directory();
        return (strpos($templatedir, 'wptouch') || strpos($templatedir, 'carrington') || strpos($templatedir, 'jetpack') || strpos($templatedir, 'handheld'));
    }

    /**
     * Skip on IP
     * @return boolean
     */
    protected function skipOnIp()
    {
        if ($this->isPluginActiveForNetwork) {
            $ipAddrs = $this->siteOptions->cache_skip_on_ip;
        } else {
            $ipAddrs = $this->options->cache_skip_on_ip;
        }
        if (empty($ipAddrs)) {
            return false;
        }
        if ($this->checkIpAddressRange(explode(PHP_EOL, $ipAddrs))) {
            return true;
        }
        return false;
    }

    /**
     * Check IP address range
     * @param array $ipAddresses
     * @return boolean
     */
    protected function checkIpAddressRange($ipAddresses = [])
    {
        if (empty($ipAddresses) || !is_array($ipAddresses)) {
            return false;
        }
        $remoteAddress = new RemoteAddress();
        $clientIpAddr = $remoteAddress->getIpAddress();
        if (empty($clientIpAddr)) {
            return false;
        }
        $ip = IP::fromStringIP($clientIpAddr);
        if ($ip->isInRanges($ipAddresses)) {
            return true;
        }
        return false;
    }

    /**
     * Check if the plugin is active for network.
     * @return boolean
     */
    public function isPluginActiveForNetwork(string $plugin): bool
    {
        include_once ABSPATH . 'wp-admin/includes/plugin.php';
        return is_plugin_active_for_network($plugin);
    }
}
