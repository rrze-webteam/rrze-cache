<?php

namespace RRZE\Cache\Utils;

defined('ABSPATH') || exit;

final class File
{
    /**
     * Write data into a file.
     * @param  string $file
     * @param  string $data
     * @return boolean
     */
    public static function write(string $file, string $data): bool
    {
        if (!$handle = @fopen($file, 'wb')) {
            return false;
        }
        @fwrite($handle, $data);
        fclose($handle);

        return self::chmod($file);
    }

    /**
     * Change the access permissions of a file.
     * @param  string $file
     * @return boolean
     */
    public static function chmod(string $file): bool
    {
        $stat = @stat(dirname($file));
        $perms = $stat['mode'] & 0007777;
        $perms = $perms & 0000666;
        $return = @chmod($file, $perms);

        return $return;
    }
}
