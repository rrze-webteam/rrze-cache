<?php

namespace RRZE\Cache;

defined('ABSPATH') || exit;

class Flush
{

    /**
     * Flush cache by post id
     * @param  integer $postId Post ID
     * @return boolean True on success, false on failure
     */
    public static function flushCacheByPostId(int $postId): bool
    {
        if (($permalink = get_permalink($postId)) === false) {
            return false;
        }
        return self::flushCacheByUrl($permalink);
    }

    /**
     * Flush cache by url
     * @param  string $url URL
     * @return boolean True on success, false on failure
     */
    public static function flushCacheByUrl(string $url): bool
    {
        return File::deleteItem($url);
    }

    /**
     * Flush all website cache
     * @return boolean True on success, false on failure
     */
    public static function flushCache(): bool
    {
        return File::clearData();
    }

    /**
     * Flush all site cache
     * @return boolean True on success, false on failure
     */
    public static function flushAllCache(): bool
    {
        return File::clearData(true);
    }
}
