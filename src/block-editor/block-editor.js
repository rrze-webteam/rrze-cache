import { __ } from "@wordpress/i18n";
import { ToggleControl } from "@wordpress/components";
import { PluginDocumentSettingPanel } from "@wordpress/edit-post";
import { registerPlugin } from "@wordpress/plugins";
import { useState } from "@wordpress/element";
import { useSelect, useDispatch } from "@wordpress/data";

const CacheComponent = () => {
    const { editPost } = useDispatch("core/editor");
    const meta = useSelect(
        (select) => select("core/editor").getEditedPostAttribute("meta"),
        []
    );
    const [isEnabled, setIsEnabled] = useState(
        meta && meta._rrze_cache === 'disabled' ? false : true
    );
    const handleToggleChange = (newValue) => {
        setIsEnabled(newValue);
        let newMeta = { ...meta, _rrze_cache: "disabled" };
        if (newValue === true) {
            newMeta._rrze_cache = "enabled";
        }
        editPost({ meta: newMeta });
    };
    return (
        <PluginDocumentSettingPanel
            name="rrze-cache-block-editor"
            title={__("Cache", "rrze-cache")}
            className="rrze-cache-block-editor"
        >
            <ToggleControl
                label={__("Enable", "rrze-cache")}
                checked={isEnabled}
                onChange={handleToggleChange}
            />
        </PluginDocumentSettingPanel>
    );
};

registerPlugin("rrze-cache-block-editor", {
    render: CacheComponent,
});
