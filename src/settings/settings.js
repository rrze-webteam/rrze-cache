jQuery(document).ready(function ($) {
    $("ul.rrze-cache-select-list").listFilterizer({
        filters: [
            {
                label: rrze_cache_vars.filters_label_1,
                selector: "*",
            },
            {
                label: rrze_cache_vars.filters_label_2,
                selector: ":has(input:checked)",
            },
        ],
        inputPlaceholder: rrze_cache_vars.placeholder,
    });
});
