jQuery(document).ready(function ($) {
    var previous = $("#rrze-cache-select").val();

    $(".edit-post-cache, .save-post-cache, .cancel-post-cache").click(
        function (e) {
            e.preventDefault();

            if ($(this).hasClass("cancel-post-cache")) {
                $("#rrze-cache-select").val(previous);
            } else if ($(this).hasClass("save-post-cache")) {
                previous = $("#rrze-cache-select").val();
                $("#post-cache-label").text(
                    $(
                        "#rrze-cache-select option[value=" +
                            $("#rrze-cache-select").val() +
                            "]"
                    ).text()
                );
                if ($("#rrze-cache-select").val() == "disabled") {
                    $("#cache-icon")
                        .removeClass("cache-icon")
                        .addClass("cache-disabled-icon");
                } else {
                    $("#cache-icon")
                        .removeClass("cache-disabled-icon")
                        .addClass("cache-icon");
                }
            }

            $("#post-cache-field").slideToggle("fast");
        }
    );
});
