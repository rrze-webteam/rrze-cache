��    +      t  ;   �      �      �  c   �     >     Y     ]     c     r     �     �     �     �     �     �     �     �     �  
   �     �     �            
        *     3     C     F     b  
   v     �     �  	   �  4   �     �     �     �  
   �  V     b   ^     �      �  2   �  !   !  �  C     	  v   8	     �	     �	     �	     �	     �	     
     
     /
  	   6
     @
     O
     e
     r
  
   ~
     �
  
   �
     �
  	   �
     �
  	   �
     �
     �
     �
     �
       
   1     <     I     _  >   h     �     �     �     �  Y   �  e   Q     �      �  2   �                           
       	               %                        #      $                                           !   "          +   &         *                        )      '                 (        %s (Access protection activated) Add one IP Address per line. The Cache will be skipped if the client has one of these IP Addresses. Advanced cache management. All Cache Cache Settings Cache is disabled Cache is empty. Cache is enabled Cache: Cancel Clear cache Delete Page Cache Disable Disabled Edit Edit Cache Enable Enable Cache Enabled Every 10 minutes Exemptions Lifetime No items found. OK Page Cache (Advanced Cache) Plugins: %1$s: %2$s RRZE Cache RRZE-Webteam Save Changes Search... Select the checkbox to remove an item from the list. Selected Settings Settings saved. Skip On IP The server is running PHP version %1$s. The plugin requires at least PHP version %2$s. The server is running WordPress version %1$s. The plugin requires at least WordPress version %2$s. Y-m-d H:i:s https://blogs.fau.de/webworking/ https://gitlab.rrze.fau.de/rrze-webteam/rrze-cache rrze-cache-expiresMinute Minutes Project-Id-Version: RRZE Cache
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/rrze-cache
PO-Revision-Date: 2025-01-21 11:17+0100
Last-Translator: RRZE Webteam <webmaster@fau.de>
Language-Team: Deutsch
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 3.5
X-Loco-Version: 2.4.4; wp-5.5.3
X-Loco-Template: languages/rrze-cache-de_DE_formal.po
 %s (Zugriffsschutz aktiviert) Fügen Sie eine IP-Adresse pro Zeile hinzu. Der Cache wird übersprungen, wenn der Client eine dieser IP-Adressen hat. Erweitertes Cache-Management. Alle Cache Einstellungen &rsaquo; Cache Cache ist deaktiviert Cache ist leer. Cache ist aktiviert Cache: Abbrechen Cache löschen Seiten-Cache löschen Deaktivieren Deaktiviert Bearbeiten Cache bearbeiten Aktivieren Cache aktivieren Aktiviert Alle 10 Minuten Ausnahmen Laufzeit Keine Elemente gefunden. OK Seiten-Cache (Advanced-Cache) Plugins: %1$s: %2$s RRZE Cache RRZE-Webteam Änderungen speichern Suche... Checkbox markieren, um ein Element aus der Liste zu entfernen. Ausgewählt Einstellungen Einstellungen gespeichert. Cache auf IP überspringen Auf dem Server läuft PHP Version %1$s. Das Plugin erfordert mindestens PHP Version %2$s. Auf dem Server läuft WordPress Version %1$s. Das Plugin erfordert mindestens WordPress Version %2$s. d.m.Y H:i:s https://blogs.fau.de/webworking/ https://gitlab.rrze.fau.de/rrze-webteam/rrze-cache Minute Minuten 